class Request {
  constructor({ url, method, headers, callback, allowDuplicate }) {
    this.url = url;
    this.method = method || 'GET';
    this.allowDuplicate = allowDuplicate || false;
    this.headers = headers || {};
    this.callback = callback || 'this.parse';
  }

  get payload() {
    return {
      url: this.url,
      method: this.method,
      allowDuplicate: this.allowDuplicate,
      headers: this.headers,
      callback: this.callback,
    };
  }
}

module.exports = Request;
