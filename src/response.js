class Response {
  constructor(data, selector, request) {
    this.data = data;
    this.selector = selector;
    this.request = request;
  }
}

module.exports = Response;
