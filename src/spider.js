const axios = require('axios');
const cheerio = require('cheerio');

const Response = require('./response');

class Spider {
  constructor() {
    this.defaults = {
      headers: {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36'
      },
    };

    this.pipelines = [];
    this.scheduledUrls = new Set();
  }

  request(req) {
    const url = req.url;

    if (! url.includes(this.allowedDomain)) {
      console.log(`Skipping not allowed domain url ${url}`);
    } else if (req.allowDuplicate && ! this.scheduledUrls.has(url)) {
      console.log(`Skipping duplicate url ${url}`);
    } else {
      this.scheduledUrls.add(url);

      setTimeout(() => {
        this.runJob(req.payload)
          .then(this.feedToPipelines.bind(this));
      }, 0);
    }
  }

  async runJob(request) {
    try {
      const {data, status} = await axios({
        method: request.method,
        url: request.url,
        headers: Object.assign(request.headers, this.defaults.headers),
      });

      if (status !== 200) {
        throw new Error('Something went wrong');
      }

      // TODO : not a correct way to pass callback
      const callback = request.callback.split('.')[1];

      return await this[callback](
        new Response(data, cheerio.load(data), request)
      );
    } catch (err) {
      this.handleErrors(err, request);
    }
  }

  crawl() {
    this.startUrls.forEach(request => {
      this.request(request);
    });
  }

  feedToPipelines(item) {
    if (! item) {
      return;
    }

    this.pipelines.forEach(pipeline => pipeline.processItem(item, this));
  }

  handleErrors(err, request) {
    console.log(`Got error ${err.message} while calling ${request.url}`);
  }
}

module.exports = Spider;
