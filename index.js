const axios = require('axios');
const cheerio = require('cheerio');

const SECTIONS_TO_SCRAPE = ['Business', 'News', 'Sports'];

axios.defaults.headers = {'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36'};
axios.defaults.baseURL = 'https://timesofindia.indiatimes.com';

(async () => {
	const {data, status} = await axios.get('/');

  if (status !== 200) {
    throw new Error('Something went wrong!');
  }

  const $ = cheerio.load(data);
  const widgets = $('.widget.fullwidth.clearfix')
  .filter((i, widget) => {
    const widgetName = $(widget).find($('ul[data-vr-zone]')).first().attr('data-vr-zone');

    return SECTIONS_TO_SCRAPE.includes(widgetName);
  })
  .each((idx, widget) => {
    const widgetName = $(widget).find($('ul[data-vr-zone]')).first().attr('data-vr-zone');

    const mainStory = $(widget).find($('.main-story')).first();
    const moreLink = $(widget).find($('.more')).first().attr('href');
    const anchorTag = mainStory.find($('a')).first();

    const relatedStories = [];

    $(widget).find($('.related-stories ul.list2 li')).each((_, el) => {
      // TODO : name the variable properly
      const a = $(el).find($('a')).first();

      relatedStories.push({
        link: a.attr('href'),
        text: a.text(),
      });
    });

    console.log({
      widgetName,
      link: anchorTag.attr('href'),
      image: anchorTag.find($('img')).first().attr('src'),
      title: anchorTag.find($('span.cp2')).first().text(),
      relatedStories,
      moreLink,
    });
  });
})();
