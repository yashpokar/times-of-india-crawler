const { ItemPipeline } = require('./src');

class MongoPipeline extends ItemPipeline {
  constructor(db) {
    super();

    this.db = db;
  }

  processItem(item, spider) {
    console.log('before storing item', item);

    this.db.items.insert(item);
  }
}

module.exports = MongoPipeline;
