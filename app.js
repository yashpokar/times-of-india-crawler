const { Spider, Request } = require('./src');
const MongoPipeline = require('./mongo-pipeline');
const mongojs = require('mongojs');

class TimesOfIndiaSpider extends Spider {
  static name = 'timesofindia';

  constructor() {
    super();

    // TODO : this should be an array but for demonstaration let it be simple
    this.allowedDomain = 'timesofindia.indiatimes.com';
    this.pipelines = [
      new MongoPipeline(
        mongojs('localhost/timesofindia', ['items'])
      ),
    ];
  }

  get startUrls() {
    return [new Request({ url: 'https://timesofindia.indiatimes.com' })];
  }

  static buildUrl(unsurePath) {
    if (unsurePath.startsWith('http')) {
      return unsurePath;
    }

    return 'https://timesofindia.indiatimes.com' + unsurePath.trim('/');
  }

  async parse(response) {
    const $ = response.selector;

    // Note : news I wan't sure about which section to pick so I left off
    const SECTIONS_TO_SCRAPE = ['Business', 'News', 'Sports'];
    const data = [];

    const widgets = $('.widget.fullwidth.clearfix')
    .filter((i, widget) => {
      const widgetName = $(widget).find($('ul[data-vr-zone]')).first().attr('data-vr-zone');

      return SECTIONS_TO_SCRAPE.includes(widgetName);
    })
    .each((idx, widget) => {
      const sectionName = $(widget).find($('ul[data-vr-zone]')).first().attr('data-vr-zone');

      const mainStory = $(widget).find($('.main-story')).first();
      const moreLink = $(widget).find($('.more')).first().attr('href');
      const anchorTag = mainStory.find($('a')).first();

      this.request(new Request({
        url: TimesOfIndiaSpider.buildUrl(moreLink),
        callback: 'this.parseAnotherRequest',
      }));

      const relatedStories = [];

      $(widget).find($('.related-stories ul.list2 li')).each((_, el) => {
        // TODO : name the variable properly
        const a = $(el).find($('a')).first();
        const link = a.attr('href');

        relatedStories.push({
          link,
          text: a.text(),
        });
      });

      data.push({
        sectionName,
        link: anchorTag.attr('href'),
        image: anchorTag.find($('img')).first().attr('src'),
        title: anchorTag.find($('span.cp2')).first().text(),
        relatedStories,
      });
    });

    return {items: data, isLandingPage: true};
  }

  parseAnotherRequest(response) {
    const $ = response.selector;

    // Note : Sorry, lying a bit because of short time left
    const text = $.text();
    const links = [];
    const images = [];

    $('a').each((_, anchorTag) => {
      let link = $(anchorTag).attr('href');

      if (link) {
        link = TimesOfIndiaSpider.buildUrl(link.trim());

        this.request(new Request({
          url: TimesOfIndiaSpider.buildUrl(link),
          callback: 'this.parseAnotherRequest',
        }));

        links.push(link);
      }
    });

    $('img').each((_, imageTag) => {
      let image = $(imageTag).attr('src');

      if (image) {
        image = TimesOfIndiaSpider.buildUrl(image.trim());
        images.push(image);
      }
    });

    return {
      links,
      images,
      text,
      currentPageUrl: response.request.url,
    };
  }
}

const spider = new TimesOfIndiaSpider();
spider.crawl();
